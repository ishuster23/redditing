//
//  HttpMethod.swift
//  Networking
//
//  Created by Alejandro Cárdenas on 04/07/20.
//  Copyright © 2020 Aleks C Barragan. All rights reserved.
//

import Foundation

public enum HttpMethod: String {
    case post = "POST"
    case get = "GET"
    public var stringValue: String {
        return rawValue
    }
}
