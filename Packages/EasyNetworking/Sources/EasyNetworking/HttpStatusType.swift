//
//  HttpStatusType.swift
//  Networking
//
//  Created by Alejandro Cárdenas on 04/07/20.
//  Copyright © 2020 Aleks C Barragan. All rights reserved.
//

import Foundation

public enum HttpStatusType {
    case undefined
    case informational
    case success
    case redirection
    case clientError
    case serverError
}
