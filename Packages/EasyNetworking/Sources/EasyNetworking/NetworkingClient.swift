//
//  NetworkingClient.swift
//  Networking
//
//  Created by Alejandro Cárdenas on 04/07/20.
//  Copyright © 2020 Aleks C Barragan. All rights reserved.
//

import Foundation

public class NetworkingClient {
    public let session: URLSession
    public let networkingConfiguration: NetworkingConfiguration
    
    public init(networkingConfiguration: NetworkingConfiguration) {
        self.networkingConfiguration = networkingConfiguration
        self.session = URLSession(configuration: networkingConfiguration.sessionConfiguration)
    }
    
    /// Request and decodes the specified type by `decodingType`.
    public func request<Model: Decodable>(
        with networkingRequest: NetworkingRequest,
        decodingType: Model.Type,
        completion: @escaping (Result<Model, NetworkingError>) -> Void) {
        let urlRequest = networkingRequest.urlRequest
        let dataTask = session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            let statusCode = (urlResponse as? HTTPURLResponse)?.statusCode ?? 999
            
            if let error = error {
                let networkError = NetworkingError(code: statusCode, error: error)
                completion(.failure(networkError))
                return
            }
            
            guard let data = data else {
                let networkError = NetworkingError(code: statusCode, error: NetworkingError.CustomError.dataIsNil)
                completion(.failure(networkError))
                return
            }
            
//            if let jsonString = String(data: data, encoding: .utf8) {
//                print(jsonString)
//            }
            
            do {
                let decoder = JSONDecoder()
                let decodedModel = try decoder.decode(decodingType.self, from: data)
                completion(.success(decodedModel))
            } catch {
                let networkError = NetworkingError(code: statusCode, error: error)
                completion(.failure(networkError))
                return
            }
        }
        
        dataTask.resume()
    }
    
    /// Request and decodes the model type.
    public func request<Model: Decodable>(
        with networkingRequest: NetworkingRequest,
        completion: @escaping (Result<Model, NetworkingError>) -> Void) {
        request(with: networkingRequest, decodingType: Model.self, completion: completion)
    }
}
