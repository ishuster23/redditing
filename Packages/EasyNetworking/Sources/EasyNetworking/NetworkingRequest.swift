//
//  NetworkingRequest.swift
//  Networking
//
//  Created by Alejandro Cárdenas on 04/07/20.
//  Copyright © 2020 Aleks C Barragan. All rights reserved.
//

import Foundation
public typealias HttpHeaders = [String: String]
public protocol NetworkingRequest {
    var baseUrlString: String { get }
    var httpMethod: HttpMethod { get }
    var httpHeaders: HttpHeaders { get }
    var httpBody: Data? { get }
    var path: String { get }
    var urlRequest: URLRequest { get }
    var parameters: [String: String] { get }
}
