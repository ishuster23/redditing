//
//  NetworkingConfiguration.swift
//  Networking
//
//  Created by Alejandro Cárdenas on 04/07/20.
//  Copyright © 2020 Aleks C Barragan. All rights reserved.
//

import Foundation

/// Configuration for NetworkingClient
public protocol NetworkingConfiguration {
    var sessionConfiguration: URLSessionConfiguration { get }
}
