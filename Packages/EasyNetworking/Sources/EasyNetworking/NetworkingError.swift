//
//  NetworkingError.swift
//  Networking
//
//  Created by Alejandro Cárdenas on 04/07/20.
//  Copyright © 2020 Aleks C Barragan. All rights reserved.
//

import Foundation

public struct NetworkingError: LocalizedError {
    public var code: Int
    public var error: Error?
    
    public var httpStatusType: HttpStatusType {
        switch code {
        case 100...110:
            return .informational

        case 200...299:
            return .success

        case 300...399:
            return .redirection

        case 400...499:
            return .clientError

        case 500...599:
            return .serverError

        default:
            return .undefined
        }
    }
    
    public var localizedDescription: String {
        return error?.localizedDescription ?? localizedStringForStatusCode
    }
    
    public var localizedStringForStatusCode: String {
        return HTTPURLResponse.localizedString(forStatusCode: code)
    }
}

public extension NetworkingError {
    enum CustomError: Swift.Error {
        case dataIsNil
    }
}
