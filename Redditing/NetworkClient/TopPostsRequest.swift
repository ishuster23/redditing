//
//  TopPostsRequest.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import EasyNetworking
import Foundation

enum TopPostsRequest: NetworkingRequest {
    case posts(after: String?)
    
    var httpMethod: HttpMethod {
        return .get
    }
    
    var httpHeaders: HttpHeaders {
        return ["Content-Type": "application/json"]
    }
    
    var httpBody: Data? {
        return nil
    }
    
    var parameters: [String : String] {
        switch self {
        case .posts(let after):
            guard let after = after else { return [:] }
            return [
                "after": after
            ]
        }
    }
    
    var path: String {
        return "/r/all/top/.json"
    }
    
    var urlRequest: URLRequest {
        guard var components = URLComponents(string: baseUrlString) else { fatalError("Will never be nil") }
        components.path = path
        var queryItems = [URLQueryItem(name: "t", value: "all"), URLQueryItem(name: "limit", value: "10")]
        queryItems += parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        components.queryItems = queryItems
        guard let url = components.url else { fatalError("This should be always a valid URL") }
        Log.debug(url.absoluteString)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        httpHeaders.forEach { (key: String, value: String) in
            urlRequest.setValue(value, forHTTPHeaderField: key)
        }
        return urlRequest
    }
    
    var baseUrlString: String {
        return "https://www.reddit.com"
    }
}
