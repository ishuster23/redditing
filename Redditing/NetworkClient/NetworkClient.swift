//
//  NetworkClient.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation
import EasyNetworking

protocol NetworkClient {
    func requestTopPosts(networkingRequest: NetworkingRequest, completion: @escaping (Result<TopPosts, NetworkingError>) -> Void)
}

extension NetworkingClient {
    /// Default NetworkClient instance to be used. Generally, it should be injected as a dependency for `NetworkClient` protocol.
    static var defaultClient: NetworkClient {
        return ConcreteNetworkClient.shared
    }
}

private struct Configuration: NetworkingConfiguration {
    let sessionConfiguration: URLSessionConfiguration
}

private final class ConcreteNetworkClient: NetworkClient {
    
    static let shared = ConcreteNetworkClient()
    
    private let appCache: AppCache
    private let networking: NetworkingClient
    
    private init() {
        let configuration = Configuration(sessionConfiguration: URLSession.shared.configuration)
        networking = NetworkingClient(networkingConfiguration: configuration)
        appCache = AppCache()
    }
    
    func requestTopPosts(networkingRequest: NetworkingRequest, completion: @escaping (Result<TopPosts, NetworkingError>) -> Void) {
        networking.request(with: networkingRequest) { [weak self] (result: Result<TopPosts, NetworkingError>) in
            guard let self = self else { return }
            let key = (networkingRequest.urlRequest.url?.absoluteString ?? #function).replacingOccurrences(of: "/", with: "-")
            switch result {
            case .success(let topPosts):
                completion(.success(topPosts))
                
            case .failure(let networkingError):
                Log.debug(networkingError)
                self.appCache.restoreData(forKey: key, type: TopPosts.self) { (cacheResult) in
                    switch cacheResult {
                    case .success(let news):
                        completion(.success(news))
                    case .failure:
                        completion(.failure(networkingError))
                    }
                }
            }
        }
    }
}
