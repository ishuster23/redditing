//
//  LoadingState.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum LoadingState {
    case loading
    case failed(message: String)
    case success
}
