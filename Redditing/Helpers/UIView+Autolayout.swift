//
//  UIView+Autolayout.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UIView {
    /// Sets `translatesAutoresizingMaskIntoConstraints` to false.
    func prepareForAutolayout() {
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    /// Uses anchors to set autolayout constraints to superview.
    /// - Parameters:
    ///   - insets: Insets to superview
    ///   - usingSafeArea: By default is `false`, set it manually to use safe area.
   @discardableResult func edgesToSuperview(insets: UIEdgeInsets = .zero, usingSafeArea: Bool = false) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        
        prepareForAutolayout()
        let superviewLeadingAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.leadingAnchor : superview.leadingAnchor
        let superviewTrailingAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.trailingAnchor : superview.trailingAnchor
        let superviewTopAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.topAnchor : superview.topAnchor
        let superviewBottomAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.bottomAnchor : superview.bottomAnchor
        
        let constraints = [
            leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: insets.left),
            trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: insets.right),
            topAnchor.constraint(equalTo: superviewTopAnchor, constant: insets.top),
            bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: insets.bottom)
        ]
        NSLayoutConstraint.activate(constraints)
        return constraints
    }
    
    @discardableResult func leadingToSuperview(offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        
        prepareForAutolayout()
        let superviewLeadingAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.leadingAnchor : superview.leadingAnchor
        let constraint = leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func leftToSuperview(offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        prepareForAutolayout()
        let superviewLeftAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.leftAnchor : superview.leftAnchor
        let constraint = leftAnchor.constraint(equalTo: superviewLeftAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func leadingToTrailingOfView(_ view: UIView, offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let viewAnchor = usingSafeArea ? view.safeAreaLayoutGuide.trailingAnchor : view.trailingAnchor
        let constraint = leadingAnchor.constraint(equalTo: viewAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func leadingToLeadingOfView(_ view: UIView, offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let viewAnchor = usingSafeArea ? view.safeAreaLayoutGuide.leadingAnchor : view.leadingAnchor
        let constraint = leadingAnchor.constraint(equalTo: viewAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func leftToRightOfView(_ view: UIView, offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let viewAnchor = usingSafeArea ? view.safeAreaLayoutGuide.rightAnchor : view.rightAnchor
        let constraint = leftAnchor.constraint(equalTo: viewAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    // MARK: - Top
    // MARK: -
    @discardableResult func topToBottomOf(_ view: UIView, offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let viewAnchor = usingSafeArea ? view.safeAreaLayoutGuide.bottomAnchor : view.bottomAnchor
        let constraint = topAnchor.constraint(equalTo: viewAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func topToSuperview(offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        
        prepareForAutolayout()
        let superviewLeadingAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.topAnchor : superview.topAnchor
        let constraint = topAnchor.constraint(equalTo: superviewLeadingAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    // MARK: - Bottom
    // MARK: -
    @discardableResult func bottomToTopOf(_ view: UIView, offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let viewAnchor = usingSafeArea ? view.safeAreaLayoutGuide.topAnchor : view.topAnchor
        let constraint = bottomAnchor.constraint(equalTo: viewAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func bottomToSuperview(offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        
        prepareForAutolayout()
        let superviewLeadingAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.bottomAnchor : superview.bottomAnchor
        let constraint = bottomAnchor.constraint(equalTo: superviewLeadingAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    // MARK: - Right side
    // MARK: -
    @discardableResult func trailingToSuperview(offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        
        prepareForAutolayout()
        let superviewLeadingAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.trailingAnchor : superview.trailingAnchor
        let constraint = trailingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func rightToSuperview(offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        prepareForAutolayout()
        let superviewLeftAnchor = usingSafeArea ? superview.safeAreaLayoutGuide.rightAnchor : superview.rightAnchor
        let constraint = rightAnchor.constraint(equalTo: superviewLeftAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func trailingToLeadingO(_ view: UIView, offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let viewAnchor = usingSafeArea ? view.safeAreaLayoutGuide.leadingAnchor : view.leadingAnchor
        let constraint = trailingAnchor.constraint(equalTo: viewAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func rightToLeftOf(_ view: UIView, offset: CGFloat = 0, usingSafeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let viewAnchor = usingSafeArea ? view.safeAreaLayoutGuide.leftAnchor : view.leftAnchor
        let constraint = rightAnchor.constraint(equalTo: viewAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    // MARK: - height
    @discardableResult func height(_ height: CGFloat, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let heightConstraint = heightAnchor.constraint(equalToConstant: height)
        heightConstraint.isActive = activate
        return heightConstraint
    }
    
    @discardableResult func heightOfView(_ view: UIView, multiplier: CGFloat, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let heightConstraint = heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: multiplier)
        heightConstraint.isActive = activate
        return heightConstraint
    }
    
    @discardableResult func width(_ width: CGFloat, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let widthConstraint = widthAnchor.constraint(equalToConstant: width)
        widthConstraint.isActive = activate
        return widthConstraint
    }
    
    @discardableResult func widthOfView(_ view: UIView, multiplier: CGFloat, activate: Bool = true) -> NSLayoutConstraint {
        prepareForAutolayout()
        let widthConstraint = widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier)
        widthConstraint.isActive = activate
        return widthConstraint
    }
    
    // MARK: - Center
    // MARK: -
    @discardableResult func centerXInSuperView(offset: CGFloat, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        
        let constraint = centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    @discardableResult func centerYInSuperView(offset: CGFloat = 0, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Before adding autolayout constraints, a superview must exists.")
        }
        
        let constraint = centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
}


