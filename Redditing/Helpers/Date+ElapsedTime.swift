//
//  Date+ElapsedTime.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

extension Date {
    func elapsedTime() -> String {
        if let hours = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour {
            let elapsedTime = hours == 1 ? "\(hours) hour ago" : "\(hours) hours ago"
            return elapsedTime
        } else {
            return "— hours ago"
        }
    }
}
