//
//  PostTableViewCell.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    let view = CellView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        let insets = UIEdgeInsets(top: 8, left: 8, bottom: -8, right: -8)
        contentView.addSubview(view)
        view.edgesToSuperview(insets: insets)
    }
}

extension PostTableViewCell {
    func configure(with viewModel: PostViewModel) {
        let placeholder = UIImage(systemName: "dot.radiowaves.left.and.right")
        view.imageView.downloadImage(with: viewModel.thumbnailUrl, placeholderImage: placeholder)
        view.nameLabel.text = viewModel.author
        view.noteLabel.text = "\(viewModel.elapsedTime) — Comments: \(viewModel.numberOfComments)"
        view.descriptionLabel.text = viewModel.title
    }
}
