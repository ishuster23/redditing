//
//  CellView.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import UIKit

/// Use it in a `UITableViewCell` or `UICollectionViewCell`
final class CellView: UIView {
    
    let backgroundView = UIView()
    let imageView = UIImageView()
    let nameLabel = UILabel()
    let noteLabel = UILabel()
    let descriptionLabel = UILabel()
    let button = UIButton()
    
    var moreInfoAction: (() -> Void)?
    
    var isNew: Bool = false {
        didSet {
            button.isHighlighted = isNew
        }
    }
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        autolayoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        autolayoutSubviews()
    }
    
    func autolayoutSubviews() {
        clipsToBounds = true
        autoresizesSubviews = true
        
        backgroundColor = UIColor.appBackgroundColor
        layer.cornerRadius = 20
        
        backgroundView.frame = self.bounds
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        backgroundView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1019638271)
        addSubview(backgroundView)
        backgroundView.edgesToSuperview()
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white
        imageView.layer.cornerRadius = 12
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1021243579)
        imageView.clipsToBounds = true
        imageView.tintColor = UIColor.appBackgroundColor
        imageView.contentMode = .center
        addSubview(imageView)
        
        let labelStackView = UIStackView()
        labelStackView.translatesAutoresizingMaskIntoConstraints = false
        labelStackView.axis = .vertical
        labelStackView.spacing = 4
        addSubview(labelStackView)
        
        nameLabel.textColor = UIColor.white
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.textAlignment = .justified
        labelStackView.addArrangedSubview(nameLabel)
        
        noteLabel.textColor = UIColor.white
        noteLabel.alpha = 0.3
        noteLabel.translatesAutoresizingMaskIntoConstraints = false
        noteLabel.numberOfLines = 2
        noteLabel.font = .preferredFont(forTextStyle: .callout)
        labelStackView.addArrangedSubview(noteLabel)
        
        addSubview(descriptionLabel)
        descriptionLabel.backgroundColor = .clear
        descriptionLabel.font = .preferredFont(forTextStyle: .body)
        descriptionLabel.textColor = .white
        descriptionLabel.alpha = 0.5
        descriptionLabel.numberOfLines = 0
        
        addSubview(button)
        let imageName = "circle.fill"
        let imageConfig = UIImage.SymbolConfiguration(scale: .large)
        let buttonImage = UIImage(systemName: imageName, withConfiguration: imageConfig)
        
        let circleImageName = "circle"
        let circleImage = UIImage(systemName: circleImageName, withConfiguration: imageConfig)
        button.tintColor = UIColor.systemBlue.withAlphaComponent(0.7)
        button.setImage(buttonImage, for: .highlighted)
        button.setImage(circleImage, for: .normal)
        button.sizeToFit()
        button.setContentHuggingPriority(.required, for: .horizontal)
        button.addTarget(self, action: #selector(actionTouchUpInside(_:)), for: .touchUpInside)
        
        let constraints = [
            imageView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            imageView.topToSuperview(offset: 8),
            imageView.widthAnchor.constraint(equalToConstant: 54.0),
            imageView.heightAnchor.constraint(equalToConstant: 54.0),
            
            button.topToSuperview(offset: 8),
            button.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -4.0),
            
            labelStackView.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8.0),
            labelStackView.trailingToLeadingO(button, offset: -8),
            labelStackView.topToSuperview(offset: 8),
            descriptionLabel.topToBottomOf(imageView, offset: 8),
            descriptionLabel.leadingToSuperview(offset: 8),
            descriptionLabel.trailingToSuperview(offset: -8),
            descriptionLabel.bottomToSuperview(offset: -16)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func actionTouchUpInside(_ sender: UIButton) {
        moreInfoAction?()
    }
    
    func configureActionButton(with image: UIImage?) {
        button.setImage(image, for: .normal)
    }
}

private extension UIColor {
    static let appBackgroundColor = #colorLiteral(red: 0.05882352941, green: 0.09019607843, blue: 0.1098039216, alpha: 1)
}
