//
//  TopPostsViewModel.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import EasyNetworking
import Foundation

class TopPostsViewModel {
    
    private let repository: PostsRepository
    private var topPost: TopPosts?
    
    private var loadingStateCompletion: ((LoadingState) -> Void)?
    var loadingState: LoadingState = .loading {
        didSet {
            DispatchQueue.main.async {
                self.loadingStateCompletion?(self.loadingState)
            }
        }
    }
    
    var alertCancelTitle: String {
        return "Cancel"
    }
    
    var alertOK: String { "OK" }
    
    var errorTitle: String {
        return "Somethig went wrong"
    }
    
    var retryActionTitle: String {
        return "Try again"
    }
    
    var navigationBarTitle: String {
        return "Top Posts"
    }
    
    var isLoading: Bool = false
    
    var after: String? {
        return topPost?.after
    }
    
    var count: Int {
        repository.count
    }
    
    // MARK: - Initis
    init(networkClient: NetworkClient) {
        self.repository = ConcretePostsRepository(networkClient: networkClient)
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        return repository.numberOfItems(inSection: section)
    }
    
    func itemAt(indexPath: IndexPath) -> PostViewModel {
        return repository.itemAt(indexpath: indexPath)
    }
    
    func isPostSeen(postViewModel: PostViewModel) -> Bool {
        return repository.isPostSeen(postViewModel: postViewModel)
    }
    
    func markPostAsSeen(postViewModel: PostViewModel) {
        repository.postSeen(postViewModel: postViewModel)
    }
    
    func loadingState(completion: @escaping (LoadingState) -> Void) {
        loadingStateCompletion = completion
    }
    
    func loadContent(after: String? = nil) {
        isLoading = true
        loadingState = .loading
        repository.loadData(after: after) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let topPosts):
                self.topPost = topPosts
                // delay to simulate some long wait.
                DispatchQueue.global().asyncAfter(deadline: .now() + 1) {
                    self.isLoading = false
                    self.loadingState = .success
                }
                
            case .failure(let networkingError):
                self.isLoading = false
                self.loadingState = .failed(message: networkingError.localizedDescription)
            }
        }
    }
    
    func remove(at indexPath: IndexPath) {
        repository.removePost(at: indexPath)
    }
    
    func removeAll() {
        repository.removeAll()
    }
}
