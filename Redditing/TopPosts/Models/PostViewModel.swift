//
//  PostViewModel.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct PostViewModel: Hashable {
    private let post: Post
    init(post: Post) {
        self.post = post
    }
    
    var author: String { post.author }
    
    var elapsedTime: String {
        return Date(timeIntervalSince1970: Double(post.created)).elapsedTime()
    }
    var title: String { post.title }
    var id: String {
        return post.id
    }
    var numberOfComments: String { "\(post.numberOfComments)"}
    var thumbnailUrl: URL? {
        return URL(string: post.thumbnail)
    }
}
