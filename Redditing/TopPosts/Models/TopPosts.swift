//
//  TopPosts.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct TopPosts: Codable, Hashable {
    let kind: String
    let distance: Int
    let after: String?
    let before: String?
    let posts: [Post]
    
    private enum CodingKeys: String, CodingKey {
        case kind, after, before
        case distance = "dist"
        case data
        case posts = "children"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        kind = try container.decode(String.self, forKey: .kind)
        
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        distance = try dataContainer.decode(Int.self, forKey: .distance)
        after = try dataContainer.decodeIfPresent(String.self, forKey: .after)
        before = try dataContainer.decodeIfPresent(String.self, forKey: .before)
        posts = try dataContainer.decode([Post].self, forKey: .posts)
    }
    
    func encode(to encoder: Encoder) throws {
        
    }
}
