//
//  Post.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct Post: Decodable, Hashable {
    let author: String
    let created: Double
    let id: String
    let title: String
    let numberOfComments: Int
    let thumbnail: String
    let thumbnailWidth: Int?
    
    private enum CodingKeys: String, CodingKey {
        case author
        case created
        case id, title, data
        case numberOfComments = "num_comments"
        case thumbnail
        case thumbnailWidth = "thumbnail_width"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        author = try dataContainer.decode(String.self, forKey: .author)
        created = try dataContainer.decode(Double.self, forKey: .created)
        id = try dataContainer.decode(String.self, forKey: .id)
        title = try dataContainer.decode(String.self, forKey: .title)
        numberOfComments = try dataContainer.decode(Int.self, forKey: .numberOfComments)
        thumbnail = try dataContainer.decode(String.self, forKey: .thumbnail)
        thumbnailWidth = try dataContainer.decodeIfPresent(Int.self, forKey: .thumbnailWidth) ?? 0
    }
}
