//
//  TopPostsTableViewController.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class TopPostsTableViewController: UITableViewController {
    
    private let networkClient: NetworkClient
    private let viewModel: TopPostsViewModel
    
    private var isLoading = false
    private var isInitialLoading = true
    
    private let activityIndicator = UIActivityIndicatorView(style: .medium)
    private lazy var activityBarButton = UIBarButtonItem(customView: activityIndicator)
    
    init(style: UITableView.Style, networkClient: NetworkClient) {
        self.networkClient = networkClient
        self.viewModel = TopPostsViewModel(networkClient: networkClient)
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        
        refreshControl = UIRefreshControl(frame: .zero)
        refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl?.attributedTitle = NSAttributedString(
            string: NSLocalizedString("Loading...", comment: "")
        )
        refreshControl?.beginRefreshing()
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(cellType: PostTableViewCell.self)
        tableView.separatorStyle = .none
        
        
        
        viewModel.loadingState { [weak self] (loadingState) in
            guard let self = self else { return }
            switch loadingState {
            case .success:
                Log.debug("success")
                self.isLoading = false
                self.reloadData()
                self.refreshControl?.endRefreshing()
                self.editButtonItem.isEnabled = true
                self.tableView.flashScrollIndicators()
                self.navigationItem.setLeftBarButton(self.editButtonItem, animated: true)
                self.activityIndicator.stopAnimating()
                
            case .loading:
                Log.debug("Loading")
                self.isLoading = true
                self.navigationItem.setLeftBarButton(self.activityBarButton, animated: true)
                self.activityIndicator.startAnimating()
                self.editButtonItem.isEnabled = false
                
                
            case .failed(let message):
                Log.debug("Failed with message: \(message)")
                self.refreshControl?.endRefreshing()
                self.isLoading = false
                self.editButtonItem.isEnabled = true
                self.navigationItem.setLeftBarButton(self.editButtonItem, animated: true)
                self.activityIndicator.stopAnimating()
            }
            
        }
        
        refresh()
    }
    
    private func configure() {
        let image = UIImage(systemName: "trash.circle.fill")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(clearData))
        navigationItem.leftBarButtonItems = [editButtonItem, activityBarButton]
        navigationItem.title = viewModel.navigationBarTitle
    }
    
    // MARK: - Actions
    @objc private func refresh() {
        viewModel.loadContent(after: viewModel.after)
    }
    
    private func reloadData() {
        DispatchQueue.main.async {
            self.tableView.isUserInteractionEnabled = false
            if self.isInitialLoading {
                let indexSet = IndexSet(integer: 0)
                self.tableView.reloadSections(indexSet, with: .fade)
                self.isInitialLoading = false
            } else {
                self.tableView.reloadData()
            }
            self.tableView.isUserInteractionEnabled = true
        }
    }
    
    @objc func clearData() {
        viewModel.removeAll()
        isInitialLoading = true
        reloadData()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems(inSection: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(for: indexPath) as PostTableViewCell
        let postViewModel = viewModel.itemAt(indexPath: indexPath)
        cell.configure(with: postViewModel)
        cell.view.isNew = !viewModel.isPostSeen(postViewModel: postViewModel)
        return cell
    }
    
    // MARK: - UITableViewDelegate
    // MARK: -
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isLoading {
            Log.debug("🔥 dont allow touches when loading")
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        let postViewModel = viewModel.itemAt(indexPath: indexPath)
        viewModel.markPostAsSeen(postViewModel: postViewModel)
        tableView.performBatchUpdates({
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }, completion: { finished in
            
        })
    }
 
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.remove(at: indexPath)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // MARK: = UIScrollViewDelegate
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let reloadDistance: CGFloat = 15.0
        if (y > (size.height + reloadDistance)) && !isLoading && size.height > 0 {
            isLoading = true
            Log.debug("🔥 load more rows")
            refresh()
        }
    }

}
