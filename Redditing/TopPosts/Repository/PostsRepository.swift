//
//  PostsRepository.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import EasyNetworking
import Foundation

protocol PostsRepository {
    init(networkClient: NetworkClient)
    var count: Int { get }
    func numberOfItems(inSection section: Int) -> Int
    func itemAt(indexpath: IndexPath) -> PostViewModel
    func loadData(after: String?, completion: @escaping (Result<TopPosts, NetworkingError>) -> Void)
    func isPostSeen(postViewModel: PostViewModel) -> Bool
    func postSeen(postViewModel: PostViewModel)
    func removePost(at indexPath: IndexPath)
    func removeAll()
}

final class ConcretePostsRepository: PostsRepository {
    let networkClient: NetworkClient
    lazy var items = [[PostViewModel]]()
    
    var seenPosts = Set<String>()
    
    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
        items.append([])
    }
    
    var count: Int {
        return items.first?.count ?? 0
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        if items.isEmpty {
            return 0
        }
        
        return items[section].count
    }
    
    func itemAt(indexpath: IndexPath) -> PostViewModel {
        return items[indexpath.section][indexpath.row]
    }
    
    func isPostSeen(postViewModel: PostViewModel) -> Bool {
        return seenPosts.contains(postViewModel.id)
    }
    
    func postSeen(postViewModel: PostViewModel) {
        seenPosts.insert(postViewModel.id)
    }
    
    func loadData(after: String? = nil, completion: @escaping (Result<TopPosts, NetworkingError>) -> Void) {
        let request = TopPostsRequest.posts(after: after)
        
        networkClient.requestTopPosts(networkingRequest: request) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let topPost):
                let postViewModels = topPost.posts.map { PostViewModel(post: $0) }
                let currentItems = (self.items.first ?? [])
                let currentItemsSet = Set(currentItems)
                let newItems = Set(postViewModels)
                let result = Array(newItems.subtracting(currentItemsSet))
                self.items = [result + currentItems]
                completion(.success(topPost))
                
            case .failure(let networkingError):
                Log.debug(networkingError.localizedDescription)
                completion(.failure(networkingError))
            }
        }
    }
    
    func removePost(at indexPath: IndexPath) {
        if items.isEmpty {
            return
        }
        
        items[indexPath.section].remove(at: indexPath.row)
    }
    
    func removeAll() {
        items = []
    }
}
