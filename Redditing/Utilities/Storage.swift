//
//  Storage.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class Storage {
    
    private init() { }
    
    enum Directory {
        /** Only documents and other data that is user-generated, or that cannot otherwise be recreated
         by your application, should be stored in the `<Application_Home>/Documents` directory and will be
         automatically backed up by iCloud.
         */
        case documents
        
        /**
         Data that can be downloaded again or regenerated should be stored
         in the `<Application_Home>/Library/Caches` directory.
         
         Examples of files you should put in the Caches directory include database cache files
         and downloadable content, such as that used by magazine, newspaper, and map applications.
         */
        case caches
    }
    
    /// Returns URL constructed from specified directory
    ///
    /// - Parameter directory: directory to get URL.
    static private func getUrl(for directory: Directory) -> URL {
        var searchPathDirectory: FileManager.SearchPathDirectory
        
        switch directory {
        case .documents:
            searchPathDirectory = .documentDirectory
        case .caches:
            searchPathDirectory = .cachesDirectory
        }
        
        if let url = FileManager.default.urls(for: searchPathDirectory, in: .userDomainMask).first {
            return url
        } else {
            print("Could not create URL for specified directory!")
            preconditionFailure()
        }
    }
    
    /// Store an encodable struct to the specified directory on disk
    ///
    /// - Parameters:
    ///   - object: the encodable struct to store
    ///   - directory: where to store the struct
    ///   - filename: what to name the file where the struct data will be stored
    static func store<T: Encodable>(_ object: T, to directory: Directory, as filename: String) {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
            }
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    /// Retrieve and convert a struct from a file on disk
    ///
    /// - Parameters:
    ///   - filename: name of the file where struct data is stored
    ///   - directory: directory where struct data is stored
    ///   - type: struct type (i.e. Message.self)
    /// - Returns: decoded struct model(s) of data
    static func retrieve<T: Decodable>(_ filename: String, from directory: Directory, as type: T.Type) -> T? {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)
        
        if !FileManager.default.fileExists(atPath: url.path) {
            print("File at path \(url.path) does not exist!")
        }
        
        if let data = FileManager.default.contents(atPath: url.path) {
            let decoder = JSONDecoder()
            do {
                let model = try decoder.decode(type, from: data)
                return model
            } catch {
                print("\(error)")
                return nil
            }
        } else {
            print("No data at \(url.path)!")
            return nil
        }
    }
    
}
