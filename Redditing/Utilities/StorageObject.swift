//
//  StorageObject.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class StorageObject {
    let data: Data
    let key: String
    init(data: Data, key: String) {
        self.data = data
        self.key = key
    }
}
