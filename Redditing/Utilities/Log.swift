//
//  Log.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum Log {
    static func debug(_ item: Any, function: String = #function, line: Int = #line, file: String = #file) {
        let name = String(file.split(separator: "/").last ?? "")
        print("\n\(line): => \(name): => \(function)\n", item)
    }
}
