//
//  AppCache.swift
//  Redditing
//
//  Created by Alejandro Cárdenas on 05/07/20.
//  Copyright © 2020 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum AppCacheError: LocalizedError {
    case notFound
    
    var localizedDescription: String {
        return "Data was not found. Probably has not ben cached or is corrupted"
    }
}

class AppCache {
    private lazy var cache = NSCache<NSString, NSData>()
    private let serialQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    func storeData<Object: Codable>(_ data: Object, key: String) {
        if let data = try? JSONEncoder().encode(data) {
            Log.debug("🦄 storing data in cache for key: \(key)")
            self.cache.setObject(data as NSData, forKey: key as NSString)
        }
    }
    
    func restoreData<Object: Codable>(forKey key: String, type: Object.Type) -> Object? {
        
        if let data = cache.object(forKey: key as NSString) {
            let object = try? JSONDecoder().decode(type, from: data as Data)
            return object
        }
        
        return nil
    }
    
    func restoreData<Object: Codable>(forKey key: String, type: Object.Type, completion: @escaping (Result<Object, Error>) -> Void) {
        serialQueue.addOperation { [unowned self] in
            do {
                
                var data: Data?
                
                if let cachedData = self.cache.object(forKey: key as NSString) as Data? {
                    data = cachedData
                } else if let savedObject = Storage.retrieve(key, from: .caches, as: Object.self) {
                    // Restore from disk
                    self.cacheData(object: savedObject, forKey: key)
                    completion(.success(savedObject))
                    return
                } else {
                    let error = AppCacheError.notFound
                    Log.debug(error)
                    completion(.failure(error))
                    return
                }
                
                guard let cachedData = data else {
                    let error = AppCacheError.notFound
                    Log.debug(error)
                    completion(.failure(error))
                    return
                }
                
                let object = try JSONDecoder().decode(type, from: cachedData)
                completion(.success(object))
                
            } catch {
                Log.debug(error)
                completion(.failure(error))
            }
        }
    }
    
    func cacheData<Object: Codable>(object: Object, forKey key: String) {
        let storeInCacheOperation = BlockOperation {
            self.storeData(object, key: key)
        }

        let storeInDiskOperation = BlockOperation {
            Storage.store(object, to: .caches, as: key)
        }
        
        serialQueue.addOperations([storeInCacheOperation, storeInDiskOperation], waitUntilFinished: false)
    }
}
